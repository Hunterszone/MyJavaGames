package util;

@FunctionalInterface
public interface Moveable {
	void move();
}
