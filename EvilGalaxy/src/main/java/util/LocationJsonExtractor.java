package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class LocationJsonExtractor {

	private static final String ACCESS_TOKEN = "?token=2760f2eddca0f4";
	private static final String IPINFO_API_ADDRESS = "https://ipinfo.io/";
	
	private LocationJsonExtractor() {}

	public static String[] extractCityAndCountry() {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONObject json = null;
		try {
			String jsonRespBlock = getJsonResponse();
			json = (org.json.simple.JSONObject) parser.parse(jsonRespBlock);
		} catch (ParseException e) {
			e.getMessage();
		} catch (IOException e) {
			e.getMessage();
		}
		
		if (json != null) {
			Object city = json.get("city");			
			Object country = json.get("country");
			return new String[] { city.toString(), country.toString() };			
		}
		
		return new String[] {"NA", "NA"};
	}

	private static String getJsonResponse() throws IOException {
		// API URL
		String sURL = new String(IPINFO_API_ADDRESS)
				.concat(getIpAddress())
				.concat(ACCESS_TOKEN);

		// GET response from API
		URL obj = new URL(sURL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.connect();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		String sResponse = response.toString();
		System.out.println("JSON response from server: " + sResponse);

		return sResponse;
	}

	private static String getIpAddress() {
		URL ipSource;
		BufferedReader in = null;
		String ip = null;
		try {
			ipSource = new URL("http://checkip.amazonaws.com");
			in = new BufferedReader(new InputStreamReader(ipSource.openStream()));
			ip = in.readLine();
		} catch (MalformedURLException e) {
			e.getMessage();
		} catch (IOException e) {
			e.getMessage();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		if (ip != null) {			
			return ip;
		} 
		return "";
	}
}
