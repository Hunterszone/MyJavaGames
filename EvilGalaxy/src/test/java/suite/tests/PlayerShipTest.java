package suite.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enemy.PlayerShip;
import game_engine.DrawScene;
import util.ActionPerformer;
import util.Constants;

public class PlayerShipTest {

	private Component component;

	@Before
	public void setUp() throws Exception {
		Constants.CITY_AND_COUNTRY = ActionPerformer.getCityAndCountry();
		component = new DrawScene();
		PlayerShip.playerShip = new PlayerShip(Constants.MYSHIP_X, Constants.MYSHIP_Y);
	}

	@Test
	public void testListsOfMissiles() {
		assertNotNull(PlayerShip.playerShip.loadMissiles());
		assertFalse(PlayerShip.playerShip.loadMissiles().isEmpty());
	}

	@Test
	public void testListsOfRockets() {
		assertNotNull(PlayerShip.playerShip.loadRockets());
		assertFalse(PlayerShip.playerShip.loadRockets().isEmpty());
	}

	@Test
	public void testMyShipPositionX() throws IOException {
		KeyEvent rightArrowKey = new KeyEvent(component, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0,
				KeyEvent.VK_RIGHT, '\u2192');
		PlayerShip.keyPressed(rightArrowKey);

		PlayerShip.playerShip.move();

		assertNotEquals("MyShip is not X-moving!", Constants.MYSHIP_X, PlayerShip.playerShip.getX());
	}

	@Test
	public void testMyShipPositionY() throws IOException {
		KeyEvent downArrowKey = new KeyEvent(component, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0,
				KeyEvent.VK_DOWN, '\u2193');
		PlayerShip.keyPressed(downArrowKey);

		PlayerShip.playerShip.move();

		assertNotEquals("MyShip is not Y-moving!", Constants.MYSHIP_Y, PlayerShip.playerShip.getY());
	}

	@After
	public void tearDown() throws Exception {
		PlayerShip.playerShip = null;
	}
}